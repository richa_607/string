var express = require('express');
var router = express.Router();

var empRouter = require('./employee');

router.use('/v1/emp', empRouter);//

module.exports = router;
var express = require('express');
var router = express.Router();
const empController = require('../controllers/employee.controller');

/**
 * @description: For Login using Mobile Number
 */
router.post('/login', empController.login);
router.post('/insert', empController.insert);
router.post('/update', empController.update);
router.post('/delete', empController.deleteemp);
router.get('/report', empController.report);
router.get('/order', empController.order);
router.get('/details', empController.details);

module.exports = router;
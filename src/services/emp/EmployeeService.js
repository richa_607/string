var sqls = require('./employeeSql');

class EmployeeService {
    /**
     * @description Dependency Injection cache and db clients
     * @constructor
     * @param {Object<Mysql>} db
     */

    constructor(db) {
        this.db = db;
    }

    async login(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const retrieveUser = sqls.retrieve();
            const userRes = await this.db.execute(retrieveUser, '', connection);
            return userRes;

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }

    }
    async insertvalue(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();
            const insertUser = sqls.insert(req);
            const userRes = await this.db.execute(insertUser, '', connection);
           let resp ={"res":{"id":userRes.insertId,msg:"Inserted Suc"}}
            return resp;

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }

    }

    async updatevalue(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const updateUser = sqls.update(req);
            const userRes = await this.db.execute(updateUser, '', connection);
            let resp ={"res":{"id":userRes.updateId,msg:"updation_success"}}
            return resp ;

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }

    }

    async delete(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const deleteUser = sqls.delete(req);
            const userRes = await this.db.execute(deleteUser, '', connection);
            let resp ={"res":{msg:"deletation_success"}}
            return resp;

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }

    }

    async report(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const retrieveUser = sqls.report(req);
            const userRes = await this.db.execute(retrieveUser, '', connection);
            let obj=[]
            userRes.map((res)=>{
                if(req)
                obj.push(res.first_name)
                console.log(res.first_name);

                if(req)
                obj.push(res.last_name)
                console.log(res.last_name);

                if(req)
                obj.push(res.dept)
                console.log(res.dept);
            })


           // let resp ={userRes}
            return {names:obj};

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }


    }
    async order(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const retrieveUser = sqls.order(req);
            const userRes = await this.db.execute(retrieveUser, '', connection);
            return userRes;

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }


    }
    
    async details(req) {
        let connection = undefined;
        try {
            connection = await this.db.getConnection();

            const retrieveUser = sqls.details(req);
            const userRes = await this.db.execute(retrieveUser, '', connection);
            return userRes;

        } catch (e) {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
            throw new Error(e);
        }
        finally {
            if (connection && connection.state === "authenticated") {
                this.closeConnect(connection, this.db);
            }
        }


    }
    

    closeConnect(connection, db) {
        process.nextTick(function () {
            db.releaseConnection(connection);
        });
    }
}
module.exports = EmployeeService;


//Retrieve User SQL Native Query
function retrieve() {
    return `select * from college.employee`;
}
module.exports.retrieve = retrieve;


function insert(req) {
    return `INSERT INTO officials.teachers (first_name,last_name,dept)
    VALUES('${req.body.fn}','${req.body.ln}','${req.body.dp}')`;
}
module.exports.insert = insert;
/*{
   "fn":"","ln":"","dp":""
    }*/

function update(req) {
    return (`UPDATE officials.teachers SET first_name='${req.body.fn}',last_name='${req.body.ln}',dept='${req.body.dp}'
    WHERE id = ${req.body.id}`)
}
module.exports.update = update;
/*{
   "fn":"","ln":"","dp":""
    }*/

function deleteemp(req) {
    return `
    DELETE FROM officials.teachers
        WHERE id = ${req.body.id}`
}
module.exports.delete = deleteemp;
/*{
    "id":"4"
    }*/

function report(req) {
    return `SELECT ${req.column} FROM officials.teachers`
}
module.exports.report = report;
/*{
    "column":"last_name"
    }*/

function order(req) {
    return `SELECT ${req.column} FROM officials.teachers ORDER BY ${req.cln} ${req.order}`
}
module.exports.order = order;
/*{
  "column":"last_name",
    "cln":"id",
"order":"asc"
    }*/


function details(req) {
    return `SELECT * FROM officials.teachers WHERE id = ${req.id}`;
}
module.exports.details= details;
/*{
    "id":"4"
    }*/
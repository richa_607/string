/**
 * @author: Anil Kumar Ch
 * @since: 23-09-2021
 * @description: Mysql credentials which will serve through out the application
 */
 module.exports = {

    config: {
        user: 'root',
        password: 'password',
        port: 3306,
        host: 'localhost',
        pageSize: 10,
        maxRows: 1000,
        poolMax: 8,
        poolTimeout: 0,
        poolPingInterval: 30,
        databases: {
            'SCHEMA': 'officials',
        },

    }
};
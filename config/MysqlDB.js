/**
 * @author: Anil Kumar Ch
 * @since: 23-09-2021
 * @description: MySql Connection establishment with pool
 */
//Brings Mysql into System
var mysql = require("mysql");
var Promise = require("es6-promise").Promise;
var dbConfig = require("./mysqlConfig");
const JSON = require('circular-json');
var pool;

/**
 * @author: Anil Kumar Ch
 * @since: 23-09-2021
 * @description: pool of connection we will maintain here
 * @param {*} config 
 */
function createPool(config) {
  return new Promise(function (resolve, reject) {
    // create connection pool
    config["typeCast"] = function (field, next) {
      // handle only BIT(1)
      if (field.type == "BIT" && field.length == 1) {
        var bit = field.string();
        return bit === null ? null : bit.charCodeAt(0);
      }
      // handle everything else as default
      return next();
    };
    pool = mysql.createPool(dbConfig.config);

    // test connection
    pool.query("select now() from dual", function (error, results, fields) {
      if (error) return reject(error);
      resolve(pool);
    });
  });
}

module.exports.createPool = createPool;

/**
 * @author: Anil Kumar Ch
 * @since: 23-09-2021
 * @description:execute Procedure call
 * @param {*} sql 
 */
function executeProcedure(sql) {
  return new Promise(function (resolve, reject) {
    // create connection pool
    pool.query(sql, function (error, results, fields) {
      if (error) return reject(error);
      resolve(results);
    });
  });

}

module.exports.executeProcedure = executeProcedure;
/**
 * @author: Anil Kumar Ch
 * @since: 23-09-2021
 * @description:terminate pool of connection
 */
function terminatePool() {
  return new Promise(function (resolve, reject) {
    if (pool) {
      pool.end(function (err) {
        if (err) {
          return reject(err);
        }

        resolve();
      });
    } else {
      resolve();
    }
  });
}

module.exports.terminatePool = terminatePool;

/**
 * @author: Anil Kumar Ch
 * @since: 23-09-2021
 * @description:fetch connection
 */
function getConnection() {
  return new Promise(function (resolve, reject) {
    pool.getConnection(function (err, connection) {
      if (err) {
        console.error("ERR1: " + err);
        return reject(err);
      }

      resolve(connection);
    });
  });
}

module.exports.getConnection = getConnection;

/**
 * @author: Anil Kumar Ch
 * @since: 23-09-2021
 * @description:release connection by passing connection object
 * @param : connection object
 */
function releaseConnection(connection) {
  connection.release();
}

module.exports.releaseConnection = releaseConnection;

/**
 * @author: Anil Kumar Ch
 * @since: 23-09-2021
 * @description:execute the sql query based on parameters
 * @param {*} sql 
 * @param {*} bindParams 
 * @param {*} connection 
 * @param {*} options 
 */
function execute(sql, bindParams, connection, options) {
  return new Promise(function (resolve, reject) {
    var data = {
      sql: updateDatabase(sql),
      values: bindParams
    };
    if (options) {
      for (var option in options) {
        data[option] = options[option];
      }
    }

    connection.query(data, function (err, results, fields) {
      if (err) {
        return reject(err);
      }

      resolve(results);
    });
  });
}

module.exports.execute = execute;

/**
 * @author: Anil Kumar Ch
 * @since: 23-09-2021
 * @description:Excutes the sql query based on parameters
 * @param {*} sql 
 * @param {*} bindParams 
 */
function simpleExecute(sql, bindParams) {
  return new Promise(function (resolve, reject) {
    getConnection()
      .then(function (connection) {
        execute(sql, bindParams, connection, {})
          .then(function (results) {

            resolve(JSON.parse(JSON.stringify(results)));

            releaseConnection(connection);
          })
          .catch(function (err) {
            reject(JSON.parse(JSON.stringify(err)));

            process.nextTick(function () {

              releaseConnection(connection);
            });
          });
      })
      .catch(function (err) {
        reject(err);
      });
  });
}

module.exports.simpleExecute = simpleExecute;

/**
 * @author: Anil Kumar Ch
 * @since: 23-09-2021
 * @description:Pagination will work here
 * @param {*} sql 
 * @param {*} pageNo 
 * @param {*} pSize 
 */
function paginateSql(sql, pageNo, pSize) {

  if (pSize === undefined) {
    pageSize = dbConfig.config.pageSize;
  } else {
    pageSize = parseInt(pSize);
  }

  var start = pageSize * (pageNo - 1);
  var stop = start + pageSize;

  var sql =
    'SELECT x.* ' +
    '   FROM (SELECT ' +
    '   a.* ' +
    '       FROM ( ' + sql + ' ) a )x' +
    '  limit ' + start + ',' + stop;

  return sql;
}

module.exports.paginateSql = paginateSql;

/**
 * @author: Anil Kumar Ch
 * @since: 23-09-2021
 * @description:Begin Connection here
 * @param {*} connection 
 */
function begin(connection) {
  return new Promise(function (resolve, reject) {
    connection.beginTransaction(function (err) {
      if (err) return reject(err);

      resolve(connection);
    });
  });
}

module.exports.begin = begin;

/**
 * @author: Anil Kumar Ch
 * @since: 23-09-2021
 * @description:Commit transaction here
 * @param {*} connection 
 */
function commit(connection) {
  return new Promise(function (resolve, reject) {
    connection.commit(function (err) {
      if (err) return reject(err);
      resolve(connection);
      process.nextTick(function () {
        releaseConnection(connection);
      });
    });
  });
}

module.exports.commit = commit;

/**
 * @author: Anil Kumar Ch
 * @since: 23-09-2021
 * @description:rollback transaction here
 * @param {*} connection 
 */
function rollback(connection) {
  return new Promise(function (resolve, reject) {
    connection.rollback(function () {
      resolve(connection);
      process.nextTick(function () {
        releaseConnection(connection);
      });
    });
  });
}

module.exports.rollback = rollback;

/** 
 * @author: Anil Kumar Ch
 * @since: 23-09-2021
 * @description:update database here based on sql query
 * @param {*} sql 
 */
function updateDatabase(sql) {
  for (var database in dbConfig.config.databases) {

    if (dbConfig.config.databases[database].length == 0) {
      sql = replaceAll(sql, "[!" + database + "!].", "");
    } else {
      sql = replaceAll(
        sql,
        "[!" + database + "!]",
        dbConfig.config.databases[database]
      );
    }
  }

  if (dbConfig.config.logSql) {
  }

  return sql;
}
/**
 * @author: Anil Kumar Ch
 * @since: 23-09-2021
 * @description:common function to replace the params
 * @param {*} str 
 * @param {*} find 
 * @param {*} replace 
 */
function replaceAll(str, find, replace) {
  return str.split(find).join(replace);
}

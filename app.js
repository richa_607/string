
const express = require('express');
// loading all models 
const app = express();
const bodyParser = require('body-parser');
const server = require('http').createServer(app);
server.listen(process.env.PORT || 6008);
app.use(bodyParser.json());
//for parsing application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended:false}));
app.use('/api', require('./routes'));


// Mysql DB connection config file
const mysqldb = require('./config/MysqlDB');
//Credentials
const dbConfig = require('./config/mysqlConfig');


//Initialize Mysql DB
initapp();
/**
 * @author: Anil Kumar Ch
 * @since : 23-09-2021
 * @description:Initialize mysql connection here
 */
function initapp() {
    mysqldb.createPool(dbConfig.config)
        .then(function () {
            console.log('Connected to MySql Successfully.');
        })
        .catch(function (err) {
            console.error('Error occurred creating database connection pool', err);
            console.log('Exiting process');
            process.exit(0);
        });
}

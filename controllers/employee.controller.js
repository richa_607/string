const EmpService = require('../src/services/emp/EmployeeService');
var mysqldb = require('../config/MysqlDB');

const login = async function (req, res) {
    try {
        const empService = new EmpService(mysqldb);
        const result = await empService.login(req.body);
        console.log("task")
      res.send(result);
    } catch (e) {
    
      res.status(500).send(Response.error(e));
    }
  };
  const insert = async function (req, res) {
    try {
        const empService = new EmpService(mysqldb);
        const result = await empService.insertvalue(req);
      res.send(result);
    } catch (e) {
    
      res.status(500).send(Response.error(e));
    }
  };
  
  const update = async function (req, res) {
    try {
        const empService = new EmpService(mysqldb);
        const result = await empService.updatevalue(req);
      res.send(result);
    } catch (e) {
    
      res.status(500).send(Response.error(e));
    }
  };
  const deleteemp = async function (req, res) {
    try {
        const empService = new EmpService(mysqldb);
        const result = await empService.delete(req);
      res.send(result);
    } catch (e) {
    
      res.status(500).send(Response.error(e));
    }
  };
  const report = async function (req,res) {
    try {
        const empService = new EmpService(mysqldb);
        const result = await empService.report(req.body);
      res.send(result);
    } catch (e) {
    
      res.status(500).send(Response.error(e));
    }
  };
  const order = async function (req,res) {
    try {
        const empService = new EmpService(mysqldb);
        const result = await empService.order(req.body);
      res.send(result);
    } catch (e) {
    
      res.status(500).send(Response.error(e));
    }
  };
  const details= async function (req,res) {
    try {
        const empService = new EmpService(mysqldb);
        const result = await empService.details(req.body);
      res.send(result);
    } catch (e) {
    
      res.status(500).send(Response.error(e));
    }
  };
 
  module.exports = {
   login,
   insert,
   update,
   deleteemp,
   report,
   order,
   details
  };